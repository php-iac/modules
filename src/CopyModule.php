<?php

namespace PHPIAC\Modules;

use PHPIAC\Connection;
use PHPIAC\Module\Module;
use PHPIAC\Modules\Support\HandlesFiles;
use PHPIAC\Modules\Support\HasPermissions;

class CopyModule extends Module
{
    use HasPermissions, HandlesFiles;

    protected string $src;
    protected string $dest;

    protected bool $force = false;
    protected bool $remoteSrc = false;

    /**
     * @inheritDoc
     */
    public function __construct(array $config)
    {
        parent::__construct($config);
    }

    /**
     * @inheritDoc
     */
    public function checkState(): bool
    {
        if ($this->force) {
            return false;
        }

        return $this->fileExists($this->dest);
    }

    /**
     * @inheritDoc
     */
    public function execute(): void
    {
        if ($this->remoteSrc) {
            Connection::exec("sudo cp -r $this->src $this->dest");
        }
        else {
            Connection::put($this->dest, $this->src);
        }

        Connection::exec($this->getPermissions($this->dest));
    }
}
