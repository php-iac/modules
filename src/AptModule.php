<?php

namespace PHPIAC\Modules;

use PHPIAC\Connection;
use PHPIAC\Module\Module;
use PHPIAC\Module\State;

class AptModule extends Module
{
    protected string $package;

    protected string $state = State::PRESENT;
    protected bool $updateCache = false;

    /**
     * @inheritDoc
     */
    public function checkState(): bool
    {
        Connection::enablePty();

        Connection::exec("dpkg -l $this->package | grep 'ii'");
        $dpkg = Connection::read();

        $state = match ($this->state) {
            State::PRESENT => str_starts_with($dpkg, 'ii'),
            State::ABSENT => str_contains($dpkg, 'no packages found'),
        };

        Connection::disablePty();

        return $state;
    }

    /**
     * @inheritDoc
     */
    public function execute(): void
    {
        if ($this->state === State::PRESENT) {
            Connection::exec("sudo apt install -y $this->package");
        }
        else if ($this->state === State::ABSENT) {
            Connection::exec("sudo apt remove -y $this->package");
        }
    }
}
