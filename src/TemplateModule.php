<?php

namespace PHPIAC\Modules;

use PHPIAC\Connection;
use PHPIAC\Module\Module;
use PHPIAC\Modules\Support\HandlesFiles;
use PHPIAC\Modules\Support\HasPermissions;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class TemplateModule extends Module
{
    use HasPermissions, HandlesFiles;

    protected string $src;
    protected string $dest;
    protected array $vars;

    protected bool $force = false;

    /**
     * @inheritDoc
     */
    public function checkState(): bool
    {
        if ($this->force) {
            return false;
            // TODO: ContentsEqual?();
        }

        return $this->fileExists($this->dest);
    }

    /**
     * @inheritDoc
     */
    public function execute(): void
    {
        $loader = new FilesystemLoader(dirname($this->src));
        $twig = new Environment($loader);
        $rendered = $twig->render(basename($this->src), $this->vars);

        Connection::put($this->dest, $rendered);

        Connection::exec($this->getPermissions($this->dest));
    }
}
