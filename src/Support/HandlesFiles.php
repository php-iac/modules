<?php

namespace PHPIAC\Modules\Support;

use PHPIAC\Connection;

trait HandlesFiles
{
    /**
     * @param string $path
     *
     * @return bool
     */
    public function fileExists(string $path): bool
    {
        Connection::enablePty();

        Connection::exec("ls $path");
        $ls = Connection::read();

        $state = ! str_contains($ls, 'No such file or directory');

        Connection::disablePty();

        return $state;
    }
}
