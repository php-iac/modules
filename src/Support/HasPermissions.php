<?php

namespace PHPIAC\Modules\Support;

trait HasPermissions
{
    protected string $owner = '';
    protected string $group = '';
    protected int $mode = 0;

    public function getPermissions($path): string
    {
        return new Permissions($path, $this->owner, $this->group, $this->mode);
    }
}
