<?php

namespace PHPIAC\Modules\Support;

class Permissions
{
    public function __construct(
        protected string $path,
        protected string $owner = '',
        protected string $group = '',
        protected int $mode = 0,
    ) {}

    public function __toString(): string
    {
        $permissions = [];

        if (! empty($this->owner) || ! empty($this->group)) {
            $permissions[] = "sudo chown -R $this->owner:$this->group $this->path";
        }

        if (! empty($this->mode)) {
            $permissions[] = "sudo chmod -R $this->mode $this->path";
        }

        return implode(PHP_EOL, $permissions);
    }
}
