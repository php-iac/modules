<?php

namespace PHPIAC\Modules;

use PHPIAC\Connection;
use PHPIAC\Module\Module;
use PHPIAC\Modules\Support\HandlesFiles;
use PHPIAC\Modules\Support\HasPermissions;

class GitModule extends Module
{
    use HasPermissions, HandlesFiles;

    protected string $repo;
    protected string $dest;

    /**
     * @inheritDoc
     */
    public function checkState(): bool
    {
        return $this->fileExists($this->dest);
    }

    /**
     * @inheritDoc
     */
    public function execute(): void
    {
        Connection::exec(
            "sudo git clone $this->repo $this->dest" . PHP_EOL .
            $this->getPermissions($this->dest)
        );
    }
}
