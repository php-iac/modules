<?php

namespace PHPIAC\Modules;

use PHPIAC\Connection;
use PHPIAC\Module\Module;
use PHPIAC\Module\State;
use PHPIAC\Modules\Support\HasPermissions;

class FileModule extends Module
{
    use HasPermissions;

    protected string $path;

    protected string $state = State::PRESENT;

    /**
     * @inheritDoc
     */
    public function checkState(): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function execute(): void
    {
        if ($this->state === State::PRESENT) {
            Connection::exec(
                "sudo touch $this->path" . PHP_EOL .
                $this->getPermissions($this->path)
            );
        }
        else {
            Connection::exec("sudo rm -rf $this->path");
        }
    }
}
