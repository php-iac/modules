<?php

namespace PHPIAC\Modules;

use PHPIAC\Connection;
use PHPIAC\Module\Module;
use PHPIAC\Module\State;

class UserModule extends Module
{
    protected string $username;
    protected string $password;

    protected bool $append = false;
    protected bool $createHome = true;
    protected array $groups = [];
    protected string $shell = '/bin/bash';
    protected string $state = State::PRESENT;

    /**
     * @inheritDoc
     */
    public function checkState(): bool
    {
        Connection::enablePty();

        Connection::exec("cat /etc/passwd | grep $this->username:");
        $hasUser = Connection::read();

        $state = match ($this->state) {
            State::PRESENT => str_starts_with($hasUser, "$this->username:"),
            State::ABSENT => empty($hasUser),
        };

        Connection::disablePty();

        return $state;
    }

    /**
     * @inheritDoc
     */
    public function execute(): void
    {
        if ($this->state === State::PRESENT) {
            Connection::exec(implode(PHP_EOL, [
                "sudo adduser $this->username --quiet" .
                " --shell " . $this->shell .
                ($this->createHome ? '' : ' --no-create-home'),
                "sudo usermod -" . ($this->append ? 'a' : '') . "G " . implode(',', $this->groups) . " $this->username"
            ]));
        }
        else if ($this->state === State::ABSENT) {
            Connection::exec("sudo userdel $this->username");
        }
    }
}
